FROM adoptopenjdk/openjdk11:alpine as build
WORKDIR /tmp/build

COPY build.gradle.kts settings.gradle.kts gradlew /tmp/build/
COPY gradle /tmp/build/gradle
COPY src/ /tmp/build/src

RUN mkdir -p src/main/resources/ && echo "spring.profiles.active=prod" > src/main/resources/application.properties
RUN mkdir -p ~/.gradle && \
    echo "org.gradle.parallel=true" >> ~/.gradle/gradle.properties

RUN ./gradlew build -x test

FROM adoptopenjdk/openjdk11:alpine

WORKDIR /app

COPY --from=build /tmp/build/build/libs/weather-api-*.jar /app/app.jar

ENTRYPOINT ["java", "-Dprofile=prod", "-jar", "/app/app.jar"]

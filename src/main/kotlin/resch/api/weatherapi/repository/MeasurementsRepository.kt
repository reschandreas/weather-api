package resch.api.weatherapi.repository

import org.springframework.data.jpa.repository.JpaRepository
import resch.api.weatherapi.model.db.Measurement
import org.joda.time.DateTime

interface MeasurementsRepository : JpaRepository<Measurement, Long> {
    fun findAllByIdAfterOrderByTimeDesc(id: Long): List<Measurement>

    fun findFirstByOrderByTimeDesc(): Measurement?

    fun findFirstByOrderByTemperatureDesc(): Measurement?

    fun findFirstByOrderByTemperatureAsc(): Measurement?

    fun findFirstByOrderByHumidityDesc(): Measurement?

    fun findFirstByOrderByHumidityAsc(): Measurement?

    fun findFirstByOrderByPressureDesc(): Measurement?

    fun findFirstByOrderByPressureAsc(): Measurement?

    fun findFirstByTimeBetweenOrderByTemperatureDesc(from: DateTime, to: DateTime): Measurement?

    fun findFirstByTimeBetweenOrderByTemperatureAsc(from: DateTime, to: DateTime): Measurement?
}
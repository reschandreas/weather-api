package resch.api.weatherapi.repository

import resch.api.weatherapi.model.ApiUser
import org.springframework.data.jpa.repository.JpaRepository

interface ApiUserRepository : JpaRepository<ApiUser, Long> {
    fun findByUsername(username: String): ApiUser
}

package resch.api.weatherapi.controller.utils

import resch.api.weatherapi.model.Role

data class CachedRole(val timestamp: Long = System.currentTimeMillis(), val roles: List<Role> = listOf()) {
    constructor(roles: List<Role>) : this(System.currentTimeMillis(), roles)
}

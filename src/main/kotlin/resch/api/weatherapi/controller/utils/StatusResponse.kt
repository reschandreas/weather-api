package resch.api.weatherapi.controller.utils

data class StatusResponse(var status: Int, var message: String?)

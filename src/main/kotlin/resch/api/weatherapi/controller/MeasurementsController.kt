package resch.api.weatherapi.controller

import org.joda.time.DateTime
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import resch.api.weatherapi.model.db.Measurement
import resch.api.weatherapi.service.ApiUserService
import resch.api.weatherapi.service.MeasurementsService
import org.joda.time.DateTimeUtils.getZone
import org.joda.time.DateTimeZone
import org.joda.time.LocalDate
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.controller
import resch.api.weatherapi.controller.utils.CachedRole

@CrossOrigin
@RestController
@RequestMapping("v1/measurements")
class MeasurementsController(private val measurementsService: MeasurementsService, val userService: ApiUserService) : BaseController(userService) {

    @GetMapping("/")
    fun getAllMeasurements(): ResponseEntity<List<Measurement>> {
        val measurements = measurementsService.getAllMeasurements();
        return ResponseEntity.ok(measurements)
    }

    @GetMapping("/now")
    fun getLatest(): ResponseEntity<Measurement> {
        val measurement = measurementsService.getLatestMeasurement()
        return returnResponse(measurement)
    }

    @GetMapping("/temperature/highest")
    fun getHighestTemperature(): ResponseEntity<Measurement> {
        val measurement = measurementsService.getHighestTemperature()
        return returnResponse(measurement)
    }

    @GetMapping("/temperature/lowest")
    fun getLowestTemperature(): ResponseEntity<Measurement> {
        val measurement = measurementsService.getLowestTemperature()
        return returnResponse(measurement)
    }

    @GetMapping("/humidity/highest")
    fun getHighestHumidity(): ResponseEntity<Measurement> {
        val measurement = measurementsService.getHighestHumidity()
        return returnResponse(measurement)
    }

    @GetMapping("/humidity/lowest")
    fun getLowestHumidity(): ResponseEntity<Measurement> {
        val measurement = measurementsService.getLowestHumidity()
        return returnResponse(measurement)
    }

    @GetMapping("/pressure/highest")
    fun getHighestPressure(): ResponseEntity<Measurement> {
        val measurement = measurementsService.getHighestPressure()
        return returnResponse(measurement)
    }

    @GetMapping("/pressure/lowest")
    fun getLowestPressure(): ResponseEntity<Measurement> {
        val measurement = measurementsService.getLowestPressure()
        return returnResponse(measurement)
    }

    @GetMapping("/temperature/highest/today")
    fun getTodaysHighestTemperature(): ResponseEntity<Measurement> {
        val list = getTodaysRange()
        val measurement = measurementsService.getHighestTemperatureOfToday(list[0], list[1])
        return returnResponse(measurement)
    }

    @GetMapping("/temperature/lowest/today")
    fun getTodaysLowestTemperature(): ResponseEntity<Measurement> {
        val list = getTodaysRange()
        val measurement = measurementsService.getLowestTemperatureOfToday(list[0], list[1])
        return returnResponse(measurement)
    }

    private fun getTodaysRange(): List<DateTime> {
        val now = DateTime.now(DateTimeZone.forID("Europe/Rome"))
        val today = now.toLocalDate()
        val tomorrow = today.plusDays(1)

        val startOfToday = today.toDateTimeAtStartOfDay(now.getZone())
        val startOfTomorrow = tomorrow.toDateTimeAtStartOfDay(now.getZone())

        return listOf(startOfToday, startOfTomorrow)
    }

    private fun returnResponse(body: Measurement?): ResponseEntity<Measurement> {
        if (body != null) {
            return ResponseEntity.ok(body)
        }
        return ResponseEntity(HttpStatus.NOT_FOUND)
    }
}
package resch.api.weatherapi.model.db

import com.fasterxml.jackson.annotation.JsonFormat
import org.hibernate.annotations.Type
import org.joda.time.DateTime
import javax.persistence.*

@Entity
@Table(name = "measurements")
data class Measurement(@Id
                       @GeneratedValue(strategy = GenerationType.SEQUENCE)
                       val id: Long,
                       @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
                       @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
                       val time: DateTime,
                       val temperature: Float,
                       val humidity: Float,
                       val pressure: Float,
                       @Column(name = "is_raining")
                       val isRaining: Boolean) {
}
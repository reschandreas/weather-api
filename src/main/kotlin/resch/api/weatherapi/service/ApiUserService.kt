package resch.api.weatherapi.service;

import resch.api.weatherapi.model.ApiUser
import resch.api.weatherapi.model.Role
import resch.api.weatherapi.repository.ApiUserRepository
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class ApiUserService(private val applicationUserRepository: ApiUserRepository) : UserDetailsService {

    fun getRoles(): List<Role> {
        val username = SecurityContextHolder.getContext().authentication.principal as String
        val user = getByUsername(username)
        return user?.roles ?: emptyList()
    }

    fun getByUsername(username: String): ApiUser? {
        return applicationUserRepository.findByUsername(username)
    }

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(s: String): UserDetails {
        return applicationUserRepository.findByUsername(s)
    }
}

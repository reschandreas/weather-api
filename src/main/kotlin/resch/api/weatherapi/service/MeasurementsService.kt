package resch.api.weatherapi.service

import org.joda.time.DateTime
import org.springframework.stereotype.Service
import resch.api.weatherapi.model.db.Measurement
import resch.api.weatherapi.repository.MeasurementsRepository

@Service
class MeasurementsService(private val measurementsRepository: MeasurementsRepository) {

    fun getAllMeasurements(): List<Measurement> {
        return measurementsRepository.findAllByIdAfterOrderByTimeDesc(0)
    }

    fun getLatestMeasurement(): Measurement? {
        return measurementsRepository.findFirstByOrderByTimeDesc()
    }

    fun getHighestTemperature(): Measurement? {
        return measurementsRepository.findFirstByOrderByTemperatureDesc();
    }

    fun getLowestTemperature(): Measurement? {
        return measurementsRepository.findFirstByOrderByTemperatureAsc();
    }

    fun getHighestHumidity(): Measurement? {
        return measurementsRepository.findFirstByOrderByHumidityDesc();
    }

    fun getLowestHumidity(): Measurement? {
        return measurementsRepository.findFirstByOrderByHumidityAsc();
    }

    fun getHighestPressure(): Measurement? {
        return measurementsRepository.findFirstByOrderByPressureDesc();
    }

    fun getLowestPressure(): Measurement? {
        return measurementsRepository.findFirstByOrderByPressureAsc();
    }

    fun getHighestTemperatureOfToday(from: DateTime, to: DateTime): Measurement? {
        return measurementsRepository.findFirstByTimeBetweenOrderByTemperatureDesc(from, to)
    }
    fun getLowestTemperatureOfToday(from: DateTime, to: DateTime): Measurement? {
        return measurementsRepository.findFirstByTimeBetweenOrderByTemperatureAsc(from, to)
    }
}